Project=Kali-Live-Build

all: build installer

install:
	   apt update && sudo apt upgrade -y
	   apt install -y git live-build simple-cdd cdebootstrap curl virt-manager ovmf

build:
	   ./build.sh --verbose --variant xfce --version 2021.4 --subdir kali-2021.4

installer:
	   ./build.sh --verbose --variant xfce --version 2021.4 --subdir kali-2021.4 --installer
